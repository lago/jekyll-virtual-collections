# Virtual collections plugin for Jekyll, version 1.1.0-beta.1
#
# This plugin is distributed under the MIT license
#
# Copyright (C) 2023 Nelson Lago <lago@ime.usp.br>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Code etc: https://gitlab.com/lago/jekyll-virtual-collections


module VirtualCollections

    class VirtualDocumentsGenerator < Jekyll::Generator
        priority :high

        # Add method "read_from_data" to Jekyll's Collection class, which
        # reads event data from a JSON, YAML, CSV etc. file in _data instead
        # of a directory. This method is inspired by the read method.
        def patch_collection
            Jekyll::Collection.class_eval {
                def read_from_data
                    if not self.metadata['virtual'] \
                                    or not site.data.key?(self.label)

                        return
                    end

                    site.data[self.label].each do |item|
                        if item.key?("path")
                            path = item["path"] # May include the extension
                        elsif item.key?("title")
                            path = item["title"]
                        else
                            # No path and no title, give up on this entry
                            Jekyll.logger.warn("Warning:", "No path and no "\
                                               "title in virtual collection "\
                                               "entry, ignoring!")
                            Jekyll.logger.warn("", "")
                            next
                        end

                        # The extension determines how Jekyll will treat
                        # the data (for example, whether to markdownify),
                        # so we always make sure there is one.
                        if item.key?("extension")
                            ext = item["extension"]
                            item.delete("extension")
                        else
                            ext = ".html"
                        end

                        if not /\.(html|htm|md)$/ =~ path
                            path = path.to_s + ext
                        end

                        item["path"] = path

                        path = collection_dir(path)
                        doc = VirtualDocument.new(path, :site => site,
                                                  :collection => self)

                        doc.read_from_data(item)

                        docs << doc if site.unpublished || doc.published?
                    end

                    sort_docs!

                    tmp = {}
                    docs.each do |doc|
                        if tmp.key?(doc.relative_path) \
                           and tmp[doc.relative_path] == false

                            Jekyll.logger.warn("Conflict:",
                                               "Two or more entries in "\
                                               "collection |#{self.label}| "\
                                               "share")
                            Jekyll.logger.warn("", "the same (virtual?) "\
                                                   "path; some of them "\
                                                   "may be ignored.")
                            Jekyll.logger.warn("", " - #{doc.relative_path}")
                            Jekyll.logger.warn("", "")
                            tmp[doc.relative_path] = true # warn only once
                        else
                            tmp[doc.relative_path] = false
                        end
                    end
                end
            }
        end

        def generate(site)
            self.patch_collection
            site.collections.each_value do |collection|
                collection.read_from_data
            end
        end

    end # class VirtualDocumentsGenerator

    # A version of Document that does not read from a file, but from the
    # given hash data instead. The "read_from_data" method is inspired
    # by the read method. We might also patch Jekyll::Document, just
    # like we did with Jekyll::Collection, but this is a little cleaner.
    class VirtualDocument < Jekyll::Document

        def read_from_data(item)
            merge_defaults

            if item.key?("content")
                self.content = item["content"]
                item.delete("content")
            else
                self.content = ""
            end
            merge_data!(item, :source => "YAML front matter")

            read_post_data
            rescue StandardError => e
                handle_read_error(e)

        end

        # Overwrite
        def source_file_mtime
            site.time
        end

    end # class VirtualDocument

end # module VirtualCollections
