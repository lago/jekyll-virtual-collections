# Virtual collections plugin for Jekyll

Ordinarily, the items in a jekyll collection are generated from the files in
the `_collection-name` directory. This plugin allows the user to add "virtual
items" to a collection by defining them in `_data/collection-name.yml` (or
json, or csv). `_data/collection-name.yml` contains an array of entries,
such as:

```
-
  title: John Doe
  job: Assistant
  content: Just someone that works here
-
  title: Jane Doe
  path: bigboss.md
  permalink: /JaneTheGreat
  job: CEO
  content: A person that works here
```

This is useful if the collection is configured with `output: true`. In
that case, these "virtual items" will be generated as independent pages,
just like the other items in the collection. Without this plugin, there
is no way to make Jekyll output a page if there is no corresponding
input file; you'd have to write some script to (1) read the data file
and (2) generate one file per entry in the `_collection-name` directory.
If you have, for example, a list of people in a .csv file, using the file
directly in Jekyll is much simpler. You may also want to collect data from
an API, such as strapi (but do note that a plugin for that already exists:
<https://github.com/strapi/jekyll-strapi/>).

If all you want is to show a list of items and not generate separate pages
for each, you do not need this plugin: instead of using a collection, you
may simply iterate over whatever is in `_data` and generate that list inside
some page (but of course you may use this plugin with `output: false` too
if you want, it's overkill but it works).

As said above, `_data/collection-name.yml` is an array of entries; each
entry defines the data that would usually be put in the file's frontmatter.
Keys `content`, `path`, `title`, and `extension` are treated specially:

 1. `content` corresponds to whatever would normally be the main content
    of the file if it existed

 2. `path` corresponds to the pathname of the file if it existed (relative
    to the collection directory and preferably including the extension)

 3. If `path` is undefined, `title` + `extension` is used instead (but see
    the warning below). If `extension` is undefined, we use `.html`.

The extension needs to always be present (either as part of `path` or
combined with `title`) because it determines how jekyll will process the
content (if it is `.md`, it markdownifies).

This was suggested here: <https://github.com/jekyll/jekyll/issues/2983>

A similar feature request: <https://github.com/jekyll/jekyll/issues/5852>

Some related discussion: <https://github.com/jekyll/jekyll/issues/5207>

## How to use this plugin

Just put it in the `_plugins` folder of your project, add `virtual: true` to
one or more of your collections and create the `_data/collection-name.yml`
files (or json, or csv, anything that Jekyll understands). Remember that
either `path` (relative to the collection directory and preferably including
the extension) or `title` *must* be defined for every entry (they will be
used to generate the corresponding filename and permalink for the entry).
If `path` is not defined, it is synthesized from `title` + `extension`;
If `extension` is also not defined, it defaults to ".html". Note that the
extension determines how Jekyll will handle whatever is in `content`
(essentially, if it is ".md" it markdownifies, if it is ".html" it does
not).

## WARNING

With a "real" collection, it is impossible for two source files to have
the exact same path. With a virtual collection, this is perfectly possible:
you may create two or more entries with the same `path` key or a single key
may be synthesized from two or more different documents with the same title
(not at all uncommon). If this happens, some of them **may** be ignored
(apparently, whether that is the case or not depends on the sorting key).
The plugin will always issue a warning when there are such repeated paths,
even if all entries are processed normally. It is a good idea to always
manually define `path` and make sure there are no clashes.

## See also

There is another plugin that also implements this feature:
<https://github.com/avillafiorita/jekyll-datapage_gen>

In comparison with jekyll-datapage\_gen, the code here just slightly changes
the default Jekyll collections, which means there is no need for special
syntax to define the template, output dir, permalinks etc.; instead, the
usual collection stuff applies. On the other hand, the code here (1) always
assumes that the name of the data file is the name of the collection, (2)
does not provide any means to programatically generate a custom title/path
for each entry and (3) does not offer filtering.

---

Code etc: <https://gitlab.com/lago/jekyll-virtual-collections>

Copyright (C) 2023 Nelson Lago <lago@ime.usp.br>

This plugin is distributed under the MIT license.
